# Nvim

![](https://raw.githubusercontent.com/theniceboy/nvim/master/demo.png)

Neovim 是 Vim 的一个分支，具有更加现代的 GUI、嵌入式以及脚本化的终端、异步工作控制等等特点，默认配置文件为~/.config/nvim/init.vim。

### 环境依赖

- [git](#)
- [ctags](https://github.com/universal-ctags/ctags.git)
- [node.js](https://github.com/nodejs/node)
  - [npm](#)
- [yarn](#)
- [ranger](#)
- [fzf](#)

### 部署步骤

#### 1. 备份你的配置文件

```bash
mv $HOME/.config/nvim $HOME/.config/nvim.bak
```

#### 2. 拉取配置文件

```bash
git clone https://gitlab.com/hoggadams/nvim $HOME/.config/
```

#### 3. 安装插件

```bash
:PlugInstall
```

### 目录结构描述

```text
.
│
├── init.vim
│
├── coc-setting.josn
│
└── plugged
    ├── nerdtree
    ├── vista.vim
    ├── vim-markdown
    ├── vim-devicon
    └── ...
```

### 键盘映射

- n <Plug>(coc-codeaction) \* :<C-U>call CocActionAsync('codeAction', '')<CR>

- n <Plug>(coc-codeaction-refactor-selected) \* :<C-U>set operatorfunc=<SNR>46_CodeActionRefactorFromSele
  cted<CR>g@

- n <Plug>(coc-codeaction-selected) \* :<C-U>set operatorfunc=<SNR>46_CodeActionFromSelected<CR>g@

- v <Plug>(coc-codeaction-refactor-selected) \* :<C-U>call CocActionAsync('codeAction', visualmode(), ['re
  factor'], v:true)<CR>

- v <Plug>(coc-codeaction-selected) \* :<C-U>call CocActionAsync('codeAction', visualmode())<CR>

- v <Plug>(coc-format-selected) \* :<C-U>call CocActionAsync('formatSelected', visualmode())<CR>

- n <Plug>(coc-codelens-action) \* :<C-U>call CocActionAsync('codeLensAction')<CR>

- n <Plug>(coc-range-select) \* :<C-U>call CocActionAsync('rangeSelect', '', v:true)<CR>

- v <Plug>(coc-range-select-backward) \* :<C-U>call CocActionAsync('rangeSelect', visualmode(), v:fals
  e)<CR>

- v <Plug>(coc-range-select) \* :<C-U>call CocActionAsync('rangeSelect', visualmode(), v:true)<CR>

- n <F3> \* :RnvimrToggle<CR>

- n <C-P> :CtrlPLine<CR>

  - <C-F> \* :Prettier<CR>

- n <C-S-Tab> :bp<CR>

- n <C-Tab> :bn<CR>

- n <C-E> :NERDTreeToggle<CR>

  - <Right> \* :vertical resize+5<CR>

  - <Left> \* :vertical resize-5<CR>

  - <Down> \* :res -5<CR>

  - <Up> \* :res +5<CR>

- n <C-H> 5h

- n <C-K> 5k

- n <C-J> 5j

- n <C-L> 5l
